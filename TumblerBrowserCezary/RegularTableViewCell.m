//
//  RegularTableViewCell.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "RegularTableViewCell.h"
#import "RegularPost.h"

@interface RegularTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

@end

@implementation RegularTableViewCell 

- (void)configureWithPost:(BasePost *)post
{
    RegularPost *regularPost = (RegularPost *)post;
    self.titleLabel.text = regularPost.regularTitle;
    
    if (regularPost.regularBody) {
        NSError *error = nil;
        self.bodyLabel.attributedText = [[NSAttributedString alloc] initWithData:[regularPost.regularBody dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:&error];
    }
}

@end
