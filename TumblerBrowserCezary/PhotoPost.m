//
//  PhotoPost.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "PhotoPost.h"
#import "NSDictionary+Common.h"

@implementation PhotoPost

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self) {
        self.type = PostTypePhoto;
        self.urlString = [dictionary valueOrNilForKey:@"photo-url-500"];
        self.width = [dictionary valueOrNilForKey:@"width"];
        self.height = [dictionary valueOrNilForKey:@"height"];
    }
    
    return self;
}

@end
