//
//  TumblrServiceInteractor.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TumblrServiceInteractorProtocol.h"
#import "TumblrServiceProtocol.h"
#import "PostResponseParserProtocol.h"

@interface TumblrServiceInteractor : NSObject <TumblrServiceInteractorProtocol>

- (instancetype)initWithService:(id<TumblrServiceProtocol>)service responseParser:(id<PostResponseParserProtocol>)responseParser;

@end
