//
//  RegularPost.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "BasePost.h"

@interface RegularPost : BasePost

@property (nonatomic, copy) NSString *regularTitle;
@property (nonatomic, copy) NSString *regularBody;

@end
