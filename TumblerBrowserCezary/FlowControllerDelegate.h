//
//  FlowControllerDelegate.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoPost.h"
#import "RegularPost.h"

@protocol FlowControllerDelegate <NSObject>

- (void)showPhotoViewControllerWithPhotoPost:(PhotoPost *)post;
- (void)showRegularViewControllerWithRegularPost:(RegularPost *)post;

@end
