//
//  PostsResponseParser.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "PostsResponseParser.h"
#import <KZAsserts/KZAsserts.h>
#import "PhotoPost.h"
#import "RegularPost.h"

@implementation PostsResponseParser

- (void)parseJSONResponse:(NSData *)response success:(PostsCompletionBlock)success failure:(FailureBlock)failure
{
    AssertTrueOr(response != nil, ;);
    AssertTrueOr(success != nil, ;);
    AssertTrueOr(failure != nil, ;);
    
    NSString *json = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    json = [json stringByReplacingOccurrencesOfString:@"var tumblr_api_read = " withString:@""];
    json = [json stringByReplacingOccurrencesOfString:@";" withString:@""];
    json = [json stringByReplacingOccurrencesOfString:@"\u00a0" withString:@"&nbsp"];
    
    NSError *error;
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
    
    if (!jsonDictionary) {
        failure(error);
        return;
    }
    
    NSArray *posts = jsonDictionary[@"posts"];
    if (posts == nil) {
        success(@[]);
        return;
    }
    
    //TODO: add mapping here
    
    NSMutableArray<BasePost *> *postsArray = [NSMutableArray array];
    
    for (NSDictionary *postDictionary in posts) {
        NSString *postType = postDictionary[@"type"];
        
        if ([postType isEqualToString:@"photo"]) {
            [postsArray addObject:[[PhotoPost alloc] initWithDictionary:postDictionary]];
        } else if ([postType isEqualToString:@"regular"]) {
            [postsArray addObject:[[RegularPost alloc] initWithDictionary:postDictionary]];
        }
    }
    
    return success(postsArray);
}

@end
