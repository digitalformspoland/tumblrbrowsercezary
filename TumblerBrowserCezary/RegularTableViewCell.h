//
//  RegularTableViewCell.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostConfigurable.h"

@interface RegularTableViewCell : UITableViewCell <PostConfigurable>

@end
