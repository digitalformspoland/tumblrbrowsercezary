//
//  ASyncConstans.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#ifndef ASyncConstans_h
#define ASyncConstans_h

#import "BasePost.h"

typedef void(^PostsCompletionBlock)(NSArray <BasePost *> *posts);
typedef void(^ServiceCompletionBlock)(id responseObject);
typedef void(^FailureBlock)(NSError *error);

#endif
