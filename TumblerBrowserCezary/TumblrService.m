//
//  TumblrService.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "TumblrService.h"
#import <AFNetworking.h>
#import <KZAsserts.h>

@implementation TumblrService

- (void)downloadPostsForTumblrUsername:(NSString *)username success:(ServiceCompletionBlock)success failure:(FailureBlock)failure
{
    AssertTrueOr(username != nil, ;);
    AssertTrueOr(success != nil, ;);
    AssertTrueOr(failure != nil, ;);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"text/json", @"text/javascript", @"application/json", @"application/javascript", nil];
    
    [manager GET:[NSString stringWithFormat:@"http://%@%@", username, @".tumblr.com/api/read/json"] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

@end
