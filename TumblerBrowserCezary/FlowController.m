//
//  FlowController.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "FlowController.h"
#import "PhotoPost.h"
#import "RegularPost.h"
#import "RegularViewController.h"
#import "PhotoViewController.h"
#import "TumblrViewController.h"
#import "TumblrViewModel.h"
#import "FlowControllerDelegate.h"

@interface FlowController () <FlowControllerDelegate>

@property (nonatomic, strong) TumblrViewModel *tumblrViewModel;

@end

@implementation FlowController

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.navigationBar.translucent = NO;
    }
    
    return self;
}

- (void)showInitialViewController
{
    [self pushViewController:[[TumblrViewController alloc] initFromNIBWithViewModel:self.tumblrViewModel] animated:YES];
}

- (void)showPhotoViewControllerWithPhotoPost:(PhotoPost *)post
{
    PhotoViewController *photoViewController = [[PhotoViewController alloc] init];
    photoViewController.photoPost = post;
    
    [self pushViewController:photoViewController animated:YES];
}

- (void)showRegularViewControllerWithRegularPost:(RegularPost *)post
{
    RegularViewController *regularViewController = [[RegularViewController alloc] init];
    regularViewController.regularPost = post;
    
    [self pushViewController:regularViewController animated:YES];
}

#pragma mark - lazy loaders

- (TumblrViewModel *)tumblrViewModel
{
    if (_tumblrViewModel == nil) {
        _tumblrViewModel = [[TumblrViewModel alloc] init];
        _tumblrViewModel.flowControllerDelegate = self;
    }
    
    return _tumblrViewModel;
}

@end
