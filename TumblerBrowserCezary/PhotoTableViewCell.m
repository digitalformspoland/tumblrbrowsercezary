//
//  PhotoTableViewCell.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "PhotoTableViewCell.h"
#import "PhotoPost.h"
#import <AFNetworking/AFNetworking.h>
#import <UIImageView+AFNetworking.h>

@implementation PhotoTableViewCell

- (void)prepareForReuse
{
    self.photoImageView.image = nil;
}

- (void)configureWithPost:(BasePost *)post
{
    PhotoPost *photoPost = (PhotoPost *)post;
    
    if (photoPost.urlString) {
        [self.photoImageView setImageWithURL:[NSURL URLWithString:photoPost.urlString]];
    }
}

@end
