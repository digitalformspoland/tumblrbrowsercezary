//
//  PhotoTableViewCell.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostConfigurable.h"

@interface PhotoTableViewCell : UITableViewCell <PostConfigurable>

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end
