//
//  TumblrServiceInteractor.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "TumblrServiceInteractor.h"

@interface TumblrServiceInteractor ()

@property (nonatomic, strong) id <TumblrServiceProtocol> service;
@property (nonatomic, strong) id <PostResponseParserProtocol> responseParser;

@end

@implementation TumblrServiceInteractor

- (instancetype)initWithService:(id<TumblrServiceProtocol>)service responseParser:(id<PostResponseParserProtocol>)responseParser
{
    self = [super init];
    
    if (self) {
        _service = service;
        _responseParser = responseParser;
    }
    
    return self;
}

- (void)downloadPostsForTumblrUsername:(NSString *)username success:(PostsCompletionBlock)success failure:(FailureBlock)failure
{
    
    
    [self.service downloadPostsForTumblrUsername:username success:^(id responseObject) {
        [self.responseParser parseJSONResponse:responseObject success:^(NSArray<BasePost *> *posts) {
            success(posts);
        } failure:^(NSError *error) {
            failure(error);
        }];
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end
