//
//  NSDictionary+Common.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Common)

- (id)valueOrNilForKey:(NSString *)key;

@end
