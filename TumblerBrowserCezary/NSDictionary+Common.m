//
//  NSDictionary+Common.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "NSDictionary+Common.h"

@implementation NSDictionary (Common)

- (id)valueOrNilForKey:(NSString *)key
{
    id value = self[key];
    if (((id)value == [NSNull null])) {
        value = nil;
    }
    return value;
}

@end
