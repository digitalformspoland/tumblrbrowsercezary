//
//  GenericPost.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "BasePost.h"
#import <KZAsserts/KZAsserts.h>

@implementation BasePost

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if (self) {
        AssertTrueOrReturnNil([dictionary isKindOfClass:[NSDictionary class]]);
    }
    
    return self;
}

@end
