//
//  PhotoViewController.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "PhotoViewController.h"
#import <UIImageView+AFNetworking.h>

@interface PhotoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (self.photoPost.urlString) {
        [self.photoImageView setImageWithURL:[NSURL URLWithString:self.photoPost.urlString]];
    }
}

@end
