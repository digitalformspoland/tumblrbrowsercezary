//
//  TumblrViewController.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "TumblrViewController.h"
#import <KZAsserts.h>
#import "TumblrViewModelDelegate.h"

@interface TumblrViewController () <TumblrViewModelDelegate>

@property (nonatomic, strong) id <TumblrViewModelProtocol> viewModel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation TumblrViewController

- (instancetype)initFromNIBWithViewModel:(id<TumblrViewModelProtocol>)viewModel
{
    self = [super initWithNibName:NSStringFromClass(self.class) bundle:nil];
    
    AssertTrueOrReturnNil(viewModel != nil);
    
    if (self) {
        _viewModel = viewModel;
    }
    
    return self;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.tableView.dataSource = self.viewModel;
    self.tableView.delegate = self.viewModel;
    self.searchBar.delegate = self.viewModel;
    self.viewModel.delegate = self;
    self.viewModel.tableView = self.tableView;
    
    [self.navigationItem setTitle:@"Tumblr posts"];
}

#pragma mark - TumblrViewModelDelegate

- (void)dataSourceHasChanged
{
    [self.tableView reloadData];
}

@end
