//
//  TumblrViewModel.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TumblrViewModelProtocol.h"
#import <UIKit/UIKit.h>
#import "TumblrServiceInteractorProtocol.h"
#import "TumblrViewModelDelegate.h"
#import "FlowControllerDelegate.h"

@interface TumblrViewModel : NSObject <TumblrViewModelProtocol, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) id <TumblrServiceInteractorProtocol> serviceInteractor;
@property (nonatomic, weak) id <TumblrViewModelDelegate> delegate;
@property (nonatomic, weak) id <FlowControllerDelegate> flowControllerDelegate;
@property (nonatomic, weak) UITableView *tableView;

@end
