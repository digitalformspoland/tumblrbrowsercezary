//
//  TumblrViewModelProtocol.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TumblrViewModelDelegate.h"
#import <UIKit/UIKit.h>

@protocol TumblrViewModelProtocol <NSObject, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, weak) id <TumblrViewModelDelegate> delegate;

@end
