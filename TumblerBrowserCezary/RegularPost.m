//
//  RegularPost.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "RegularPost.h"
#import "NSDictionary+Common.h"

@implementation RegularPost

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    
    if (self) {
        self.type = PostTypeRegular;
        self.regularTitle = [dictionary valueOrNilForKey:@"regular-title"];
        self.regularBody = [dictionary valueOrNilForKey:@"regular-body"];
    }
    
    return self;
}

@end
