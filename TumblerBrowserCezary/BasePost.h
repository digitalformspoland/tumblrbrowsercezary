//
//  GenericPost.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParsablePost.h"

typedef NS_ENUM (NSInteger, PostType) {
    PostTypePhoto = 1,
    PostTypeRegular = 2
};

@interface BasePost : NSObject <ParsablePost>

@property (nonatomic, assign) PostType type;

@end
