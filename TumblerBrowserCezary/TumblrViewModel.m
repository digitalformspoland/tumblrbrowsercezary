//
//  TumblrViewModel.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import "TumblrViewModel.h"
#import "TumblrServiceInteractor.h"
#import "TumblrService.h"
#import "PostsResponseParser.h"
#import "PhotoTableViewCell.h"
#import "PostConfigurable.h"
#import "MMProgressHUD.h"
#import "BasePost.h"
#import "RegularTableViewCell.h"

@interface TumblrViewModel ()

@property (nonatomic, copy) NSArray *dataSource;

@end

@implementation TumblrViewModel

#pragma mark - Initializers

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        [[MMProgressHUD sharedHUD] setPresentationStyle:MMProgressHUDPresentationStyleFade];
        [[MMProgressHUD sharedHUD] setOverlayMode:MMProgressHUDWindowOverlayModeGradient];
    }
    
    return self;
}

#pragma mark - Setters

- (void)setTableView:(UITableView *)tableView
{
    _tableView = tableView;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PhotoTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PhotoTableViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RegularTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RegularTableViewCell class])];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BasePost *post = self.dataSource[indexPath.row];
    
    id <PostConfigurable> cell = [self.tableView dequeueReusableCellWithIdentifier:[self tableViewCellIdentifierFromType:post.type] forIndexPath:indexPath];
    
    [cell configureWithPost:post];
    
    return (UITableViewCell *)cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BasePost *basePost = self.dataSource[indexPath.row];
    
    switch (basePost.type) {
        case PostTypePhoto:
            [self.flowControllerDelegate showPhotoViewControllerWithPhotoPost:(PhotoPost *)basePost];
            break;
            
        case PostTypeRegular:
            [self.flowControllerDelegate showRegularViewControllerWithRegularPost:(RegularPost *)basePost];
            break;
            
        default:
            break;
    }
}

#pragma mark - Helper methods

- (NSString *)tableViewCellIdentifierFromType:(PostType)type
{
    switch (type) {
        case PostTypeRegular:
            return NSStringFromClass([RegularTableViewCell class]);
            break;
            
        case PostTypePhoto:
            return NSStringFromClass([PhotoTableViewCell class]);
            break;
            
        default:
            break;
    }
    
    return @"";
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    if (![searchBar.text isEqualToString:@""]) {
        [self downloadPostsForUsername:searchBar.text];
    }
}

#pragma mark - Service methods

- (void)downloadPostsForUsername:(NSString *)username
{
    [MMProgressHUD show];
    [self.serviceInteractor downloadPostsForTumblrUsername:username success:^(NSArray<BasePost *> *posts) {
        self.dataSource = posts;
        [self.delegate dataSourceHasChanged];
        [MMProgressHUD dismiss];
    } failure:^(NSError *error) {
        [MMProgressHUD dismissWithError:error.description title:[NSString stringWithFormat:@"Error %ld", (long)error.code] afterDelay:1.0];
    }];
}

#pragma mark - Lazy loaders

- (id<TumblrServiceInteractorProtocol>)serviceInteractor
{
    if (_serviceInteractor == nil) {
        _serviceInteractor = [[TumblrServiceInteractor alloc] initWithService:[[TumblrService alloc] init] responseParser:[[PostsResponseParser alloc] init]];
    }
    
    return _serviceInteractor;
}

@end
