//
//  PhotoViewController.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoPost.h"

@interface PhotoViewController : UIViewController

@property (nonatomic, strong) PhotoPost *photoPost;

@end
