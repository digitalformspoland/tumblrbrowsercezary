//
//  PostResponseParserProtocol.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASyncConstans.h"

@protocol PostResponseParserProtocol <NSObject>

- (void)parseJSONResponse:(NSData *)response success:(PostsCompletionBlock)success failure:(FailureBlock)failure;

@end
