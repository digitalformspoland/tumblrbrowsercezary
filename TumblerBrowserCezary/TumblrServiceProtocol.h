//
//  TumblrServiceProtocol.h
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 07/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASyncConstans.h"

@protocol TumblrServiceProtocol <NSObject>

- (void)downloadPostsForTumblrUsername:(NSString *)username success:(ServiceCompletionBlock)success failure:(FailureBlock)failure;

@end
