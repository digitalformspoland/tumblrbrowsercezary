//
//  ServiceTest.m
//  TumblerBrowserCezary
//
//  Created by CezaryBielecki on 08/04/16.
//  Copyright © 2016 CezaryBielecki. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OHHTTPStubs.h"
#import "TumblrService.h"

NSString* const kTestServiceFailure = @"failServiceURL";
NSString* const kTestServiceSuccess = @"successServiceURL";

@interface ServiceTest : XCTestCase

@property (nonatomic, strong) TumblrService *sut;

@end

@implementation ServiceTest

- (void)setUp
{
    [super setUp];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [[request.URL absoluteString] isEqualToString:[NSString stringWithFormat:@"http://%@.tumblr.com/api/read/json", kTestServiceFailure]];
    } withStubResponse:^OHHTTPStubsResponse *(NSURLRequest *request) {
        return [OHHTTPStubsResponse responseWithData:[NSData data] statusCode:404 headers:nil];
    }];
    
    [OHHTTPStubs stubRequestsPassingTest: ^BOOL (NSURLRequest *request) {
        return [[request.URL absoluteString] isEqualToString:[NSString stringWithFormat:@"http://%@.tumblr.com/api/read/json", kTestServiceSuccess]];
    } withStubResponse: ^OHHTTPStubsResponse *(NSURLRequest *request) {
        return [OHHTTPStubsResponse responseWithData:[NSData data] statusCode:200 headers:nil];
    }];
    
    self.sut = [[TumblrService alloc] init];
}

- (void)tearDown
{
    self.sut = nil;
    [OHHTTPStubs removeAllStubs];
    [super tearDown];
}

- (void)testThatServiceShouldReturnError
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"Testing failure"];
    
    void(^successTest)(id) = ^ (id response){
        XCTAssertNotNil(response, @"Response should be not nil");
        XCTAssertTrue([response isKindOfClass:[NSError class]], @"Response should be error kind of class");
    };
    
    [self.sut downloadPostsForTumblrUsername:kTestServiceFailure success:^(id responseObject) {
        [expectation fulfill];
        successTest(responseObject);
    } failure:^(NSError *error) {
        [expectation fulfill];
        successTest(error);
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:nil];
}

- (void)testThatServiceShouldReturnParsedResponse
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"Testing completion"];
    
    void(^successTest)(id) = ^ (id response){
        XCTAssertFalse([response isKindOfClass:[NSError class]], @"Response should not be error kind of class");
    };
    
    [self.sut downloadPostsForTumblrUsername:kTestServiceSuccess success:^(id responseObject) {
        [expectation fulfill];
        successTest(responseObject);
    } failure:^(NSError *error) {
        [expectation fulfill];
        successTest(error);
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:nil];
}

@end
